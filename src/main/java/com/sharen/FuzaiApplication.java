package com.sharen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FuzaiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FuzaiApplication.class, args);
	}
}
