package com.sharen.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by wangxiaotao on 17/8/16.
 */
@Controller
@RequestMapping("user")
public class SRUserController {

    @RequestMapping("hello")
    @ResponseBody
    public String hello(){
        return "hello world" ;
    }
}
